import axios from 'axios'

let user = { roles: [], username: '', authenticated: false }
export default {
  login: function (context, username, password, redirect) {
    let token = btoa(username + ':' + password)
    axios.get('http://localhost:8090/api/authenticate', { headers: { 'Authorization': `Basic ${token}` } })
      .then(response => {
        user.username = username
        user.roles = response.data
        console.log(user.roles)
        user.authenticated = true
        window.localStorage.setItem('token-' + username, token)
        if (redirect) {
          context.$router.push({ path: redirect })
        }
      })
      .catch(error => {
        console.log(error)
      })
  },
  hasAnyOf: function (roles) {
    return !!user.roles.find(role => roles.includes(role))
  },
  logout: function () {
    window.localStorage.removeItem('token-' + user.username)
    user = { roles: [], username: '', authenticated: false }
  },
  authenticated: function () {
    return user.authenticated
  },
  getAuthHeader: function () {
    return {
      'Authorization': `Basic ${window.localStorage.getItem('token-' + user.username)}`
    }
  }
}
