import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './components/auth/Login.vue'
import auth from './components/auth/auth.js'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: (_to, _from, next) => {
        if (!auth.authenticated()) {
          next({ path: '/login' })
        } else {
          next()
        }
      }
    },
    { path: '/login', name: 'login', component: Login },
    {
      path: '/logout',
      name: 'logout',
      component: Login,
      beforeEnter: (_to, _from, next) => {
        auth.logout()
        next({ path: '/login' })
      }
    }
  ]
})
